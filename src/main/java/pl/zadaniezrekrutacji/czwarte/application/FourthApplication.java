package pl.zadaniezrekrutacji.czwarte.application;

import java.io.IOException;

public class FourthApplication {

	private static String clearCode;
	private static int temp;
	private static char num;

	public static String checkControlNumberEAN8(String code) throws IOException { // EAN8
		int sum = 0;
		if (code.length() == 7 || code.length() == 9 || code.length() == 12) {
			code = 0 + code;
		}
		if (code.length() == 8 || code.length() == 10 || code.length() == 13) {

			clearCode = "";
			int controlNumber = Integer.parseInt(code.substring(7, 8)); // cyfra kontrolna na 8 miejscu

			for (int i = 0; i < 7; i++) {
				num = code.charAt(i);
				temp = Character.getNumericValue(num); // obliczanie sumy kontrolnej
				if (i % 2 != 0) {
					temp = temp * 3;
				}
					sum += temp;
			}
			if (sum % 10 != controlNumber) { // porownanie sumy z cyfra
				throw new IOException("Nie zgadza sie suma kontrolna EAN8 " + controlNumber + " " + sum % 10);

			} else
				clearCode = code.substring(0, 8);
			return clearCode;
			// return Integer.parseInt(code.substring(0, 8));
		} else
			throw new IOException("Kod o niewlasciwej dlugosci EAN8: " + code.length());
	}

	public static String checkControlNumberEAN13(String code) throws IOException { // EAN13
		int sum = 0;
		if (code.length() == 12 || code.length() == 14 || code.length() == 17) {
			code = 0 + code;
		}
		if (code.length() == 13 || code.length() == 15 || code.length() == 18) {
			clearCode = "";
			int controlNumber = Integer.parseInt(code.substring(12, 13)); // cyfra kontrolna na 13 miejscu

			for (int i = 0; i < 12; i++) {
				num = code.charAt(i);
				temp = Character.getNumericValue(num); // obliczanie sumy kontrolnej
				if (i % 2 != 0) {
					temp = temp * 3;
				}
				sum += temp;
			}
			if (sum % 10 != controlNumber) { // porownanie sumy z cyfra
				throw new IOException("Nie zgadza sie suma kontrolna EAN13 " + controlNumber + " " + sum % 10);

			} else
				clearCode = code.substring(0, 13);
			return clearCode;
		} else
			throw new IOException("Kod o niewlasciwej dlugosci EAN13 " + code.length());
	}

	public static String verifyCode(String code, int EAN) throws IOException {
		if(EAN < 1 || EAN > 2) {
			throw new Exception("Nieprawidlowe oznaczenie kodu EAN");
		}
		if (EAN == 1) {
			return checkControlNumberEAN8(code);
		} else
			return checkControlNumberEAN13(code);
	}

	public static void main(String[] args) throws IOException {
		System.out.println(verifyCode("55123457", 1));
		System.out.println(verifyCode("0075678164125", 2));

	}

}
